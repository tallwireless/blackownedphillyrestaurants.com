function mapdata(data, color, map, infowindow) {
    var marker, count;
    let url = "http://maps.google.com/mapfiles/ms/icons/";
      url += color + "-dot.png";

    for (count = 0; count < data.length; count++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(data[count][2], data[count][3]),
          map: map,
          title: data[count][0],
          icon: { url }
        });
    google.maps.event.addListener(marker, 'click', (function (marker, count) {
          return function () {
            infowindow.setContent(data[count][1]);
            infowindow.open(map, marker);
          }
        })(marker, count));
      }
}



function initMap() {

var map = new google.maps.Map(document.getElementById('map'), { zoom: 12, center:{lat: 39.957691, lng: -75.162837} });
var infowindow =  new google.maps.InfoWindow({});

mapdata(philly,"red",map,infowindow);
mapdata(sj,"red",map,infowindow);
mapdata(businesses,"blue",map,infowindow);

    var field = document.getElementById("update");
    field.innerHTML += update;

}
